package de.itlobby.remoteJux.services;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.models.CLICommand;
import de.itlobby.remoteJux.models.CLIResponse;
import de.itlobby.remoteJux.settings.Settings;
import de.itlobby.remoteJux.util.ExceptionUtil;
import de.itlobby.remoteJux.util.MapUtil;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class BackendService extends AsyncTask<CLICommand, Void, CLIResponse>
{
    private static BackendService instance;

    private BackendService()
    {
    }

    public static BackendService getInstance()
    {
        if (instance == null)
        {
            instance = new BackendService();
        }

        return instance;
    }

    @Override
    public CLIResponse doInBackground(CLICommand... command)
    {
        CLIResponse cliResponse = restPOST(command[0], "execute", CLIResponse.class);

        Toast.makeText(Settings.getInstance().getAppContext(), Settings.getInstance().getAppContext().getString(R.string.backendService_command) + " " +
                           command[0].getCommand() +
                           Settings.getInstance().getAppContext().getString(R.string.backendService_execCommand), Toast.LENGTH_SHORT
        ).show();

        return cliResponse;
    }

    private <T> T restPOST(CLICommand cliCommand, String webMethodName, Class<T> returnType)
    {
        try
        {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpost = new HttpPost(getBaseUrl() + webMethodName);
            JSONObject holder = new JSONObject(MapUtil.toJSON(cliCommand));
            StringEntity se = new StringEntity(holder.toString());
            httpost.setEntity(se);

            httpost.setHeader("Accept", "application/json");
            httpost.setHeader("Content-type", "application/json");

            ResponseHandler responseHandler = new BasicResponseHandler();
            String responseString = (String) httpclient.execute(httpost, responseHandler);

            return MapUtil.toObject(responseString, returnType);
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }

        return null;
    }

    private String getBaseUrl()
    {
        Settings settings = Settings.getInstance();
        String servername = settings.getConfig().getServerName();
        String port = settings.getConfig().getServerPort();

        return "http://" + servername + ":" + port + "/cmd/";
    }

    public boolean checkWebService()
    {
        boolean isOnline;

        try
        {
            String pingUrl = getBaseUrl() + "ping";

            Log.v("INFO", "ping URL " + pingUrl);

            URL url = new URL(pingUrl);
            URLConnection con = url.openConnection();
            con.setConnectTimeout(1000);
            con.setReadTimeout(1000);

            InputStream in = con.getInputStream();

            isOnline = true;
        }
        catch (Exception e)
        {
            isOnline = false;
        }

        Log.v("INFO", "ping result " + isOnline);

        return isOnline;
    }
}
