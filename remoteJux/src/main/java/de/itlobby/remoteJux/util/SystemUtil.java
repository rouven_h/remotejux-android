package de.itlobby.remoteJux.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import de.itlobby.remoteJux.activities.AddCommandActivity;
import de.itlobby.remoteJux.models.IconInfo;

import java.util.ArrayList;
import java.util.List;

public class SystemUtil
{
    public static List<IconInfo> getAllInstalledAppIcons(AddCommandActivity activity)
    {
        List<IconInfo> icons = new ArrayList<>();

        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> pkgAppsList = activity.getApplicationContext().getPackageManager().queryIntentActivities(mainIntent, 0);

        for (ResolveInfo resolveInfo : pkgAppsList)
        {
            String appName = (String) activity.getBaseContext().getPackageManager().getApplicationLabel(resolveInfo.activityInfo.applicationInfo);

            IconInfo iconInfo = new IconInfo();
            iconInfo.setAppPackageName(resolveInfo.activityInfo.packageName);
            iconInfo.setIconResourceId(resolveInfo.getIconResource());
            iconInfo.setName(appName);

            icons.add(iconInfo);
        }

        return icons;
    }

    public static Drawable getDrawableFromApp(PackageManager packageManager, String packageName, int resId)
    {
        try
        {
            return packageManager.getResourcesForApplication(packageName).getDrawable(resId);
        }
        catch (PackageManager.NameNotFoundException e)
        {
            ExceptionUtil.logException(e);
        }

        return null;
    }

    public static Drawable getDrawableFromApp(Context context, IconInfo iconInfo)
    {
        try
        {
            if (iconInfo == null)
            {
                return new ColorDrawable(Color.TRANSPARENT);
            }

            return context.getPackageManager().getResourcesForApplication(iconInfo.getAppPackageName()).getDrawable(iconInfo.getIconResourceId());
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
            return new ColorDrawable(Color.TRANSPARENT);
        }
    }

}
