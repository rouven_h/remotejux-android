package de.itlobby.remoteJux.util;

import de.itlobby.remoteJux.models.CLICommandWrapper;
import de.itlobby.remoteJux.models.WidgetEntry;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapUtil
{

    public static String toJSON(Object object)
    {
        if (object.getClass().equals(String.class))
        {
            return (String) object;
        }

        OutputStream outputStream = null;

        try
        {
            ObjectMapper mapper = new ObjectMapper();

            outputStream = new ByteArrayOutputStream();

            mapper.writeValue(outputStream, object);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return outputStream.toString();
    }

    public static <T> T toObject(String jsonString, Class<T> targetClass)
    {
        if (targetClass.equals(String.class))
        {
            return (T) jsonString;
        }

        try
        {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonString, targetClass);
        }
        catch (IOException e)
        {
            ExceptionUtil.logException(e);
        }

        return null;
    }

    static String readAll(Reader rd) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1)
        {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static String readJsonContentFromUrl(String baseUrl, String methodName)
    {
        try
        {
            InputStream is = new URL(baseUrl + methodName).openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            return readAll(rd);
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
            return "";
        }
    }

    public static String createJsonParamArray(Object... objects)
    {
        String[] jsonParamArray = new String[objects.length];

        for (int i = 0; i < objects.length; i++)
        {
            Object object = objects[i];

            jsonParamArray[i] = toJSON(object);
        }

        return toJSON(jsonParamArray);
    }

    public static Set<String> mapCommandsToJsonCommands(List<CLICommandWrapper> commands)
    {
        Set<String> ret = new HashSet<>();

        for (CLICommandWrapper command : commands)
        {
            ret.add(toJSON(command));
        }

        return ret;
    }

    public static List<CLICommandWrapper> mapJsonToCommands(Set<String> commandSet)
    {
        List<CLICommandWrapper> ret = new ArrayList<>();

        for (String json : commandSet)
        {
            ret.add(toObject(json, CLICommandWrapper.class));
        }

        return ret;
    }

    public static Set<String> mapWidgetsToJsonWidgets(HashMap<Integer, CLICommandWrapper> widgets)
    {
        Set<String> ret = new HashSet<>();

        for (Map.Entry<Integer, CLICommandWrapper> entry : widgets.entrySet())
        {
            ret.add(toJSON(new WidgetEntry(entry)));
        }

        return ret;
    }

    public static HashMap<Integer, CLICommandWrapper> mapJsonToWidgets(Set<String> widgets)
    {
        HashMap<Integer, CLICommandWrapper> ret = new HashMap<>();

        for (String widget : widgets)
        {
            WidgetEntry entry = toObject(widget, WidgetEntry.class);
            ret.put(entry.getKey(), entry.getValue());
        }

        return ret;
    }
}
