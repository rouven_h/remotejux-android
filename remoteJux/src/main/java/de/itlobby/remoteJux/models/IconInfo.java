package de.itlobby.remoteJux.models;

import java.io.Serializable;

public class IconInfo implements Serializable
{
    private String name;
    private String appPackageName;
    private Integer iconResourceId;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAppPackageName()
    {
        return appPackageName;
    }

    public void setAppPackageName(String appPackageName)
    {
        this.appPackageName = appPackageName;
    }

    public Integer getIconResourceId()
    {
        return iconResourceId;
    }

    public void setIconResourceId(Integer iconResourceId)
    {
        this.iconResourceId = iconResourceId;
    }
}
