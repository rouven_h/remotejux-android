package de.itlobby.remoteJux.models;

public enum NotifyType
{
    ADD,
    EDIT,
    REMOVE
}
