package de.itlobby.remoteJux.models;

import de.itlobby.remoteJux.util.MapUtil;

import java.io.Serializable;

public class CLICommandWrapper implements Serializable
{
    private CLICommand cliCommand;

    private String displayValue;
    private IconInfo iconInfo;

    public CLICommandWrapper()
    {
    }

    public CLICommandWrapper(CLICommand cliCommand)
    {
        this.cliCommand = cliCommand;
    }

    public CLICommand getCliCommand()
    {
        return cliCommand;
    }

    public void setCliCommand(CLICommand cliCommand)
    {
        this.cliCommand = cliCommand;
    }

    public String getDisplayValue()
    {
        return displayValue;
    }

    public void setDisplayValue(String displayValue)
    {
        this.displayValue = displayValue;
    }

    @Override
    public String toString()
    {
        return displayValue;
    }

    public IconInfo getIconInfo()
    {
        return iconInfo;
    }

    public void setIconInfo(IconInfo iconInfo)
    {
        this.iconInfo = iconInfo;
    }

    @Override
    public int hashCode()
    {
        return MapUtil.toJSON(this).hashCode();
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == null)
        {
            return false;
        }

        String thisString = MapUtil.toJSON(this);
        String otherString = MapUtil.toJSON(other);

        return thisString.equals(otherString);
    }
}
