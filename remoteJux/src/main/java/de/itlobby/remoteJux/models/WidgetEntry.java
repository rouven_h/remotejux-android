package de.itlobby.remoteJux.models;

import java.io.Serializable;
import java.util.Map;

public class WidgetEntry implements Serializable
{
    private Integer key;
    private CLICommandWrapper value;

    public WidgetEntry()
    {
    }

    public WidgetEntry(Map.Entry<Integer, CLICommandWrapper> entry)
    {
        this.key = entry.getKey();
        this.value = entry.getValue();
    }

    public Integer getKey()
    {
        return key;
    }

    public void setKey(Integer key)
    {
        this.key = key;
    }

    public CLICommandWrapper getValue()
    {
        return value;
    }

    public void setValue(CLICommandWrapper value)
    {
        this.value = value;
    }
}
