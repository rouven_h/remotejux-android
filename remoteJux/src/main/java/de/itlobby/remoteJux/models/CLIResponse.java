package de.itlobby.remoteJux.models;

import java.io.Serializable;

public class CLIResponse implements Serializable
{
    private String errorMessage;
    private boolean hasError;
    private String result;

    public CLIResponse()
    {
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public Boolean getHasError()
    {
        return hasError;
    }

    public void setHasError(Boolean hasError)
    {
        this.hasError = hasError;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }
}
