package de.itlobby.remoteJux.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.adapters.ComboIconArrayAdapter;
import de.itlobby.remoteJux.listener.EventProvider;
import de.itlobby.remoteJux.models.IconInfo;

import java.util.List;

public class ComboDialog
{
    public static void showIconSelctor(Context baseActivity, String title, final List<IconInfo> icons)
    {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.setContentView(R.layout.combo_box_base_layout);
        dialog.setTitle(title);

        ListView listView = (ListView) dialog.findViewById(R.id.lwIcons);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                                        {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                                            {
                                                IconInfo selected = icons.get(position);
                                                EventProvider.iconSelectorSelectEvent.itemSelected(selected);
                                                dialog.dismiss();
                                            }
                                        }
        );
        ComboIconArrayAdapter adapter = new ComboIconArrayAdapter(baseActivity, icons);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        dialog.show();
    }
}
