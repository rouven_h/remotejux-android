package de.itlobby.remoteJux.dialogs;

public enum ContextMenuItem
{
    EDIT(0),
    DELETE(1);

    private final int position;

    ContextMenuItem(int position)
    {
        this.position = position;
    }

    public static ContextMenuItem getItemAt(int index)
    {
        for (ContextMenuItem contextMenuItem : values())
        {
            if (index == contextMenuItem.getPosition())
            {
                return contextMenuItem;
            }
        }

        return null;
    }

    public int getPosition()
    {
        return position;
    }
}
