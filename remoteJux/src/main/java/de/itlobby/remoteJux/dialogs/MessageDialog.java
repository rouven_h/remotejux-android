package de.itlobby.remoteJux.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.listener.DialogButtonClickListener;

public class MessageDialog
{
    static DialogInterface.OnClickListener dismissListener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            dialog.dismiss();
        }
    };

    public static void show(Context baseActivity, String title, String message, final DialogButtonClickListener okButtonListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);

        builder.setMessage(message).setTitle(title);

        builder.setPositiveButton(baseActivity.getString(R.string.dialog_ok), new DialogInterface.OnClickListener()
                                  {
                                      public void onClick(DialogInterface dialog, int id)
                                      {
                                          okButtonListener.onButtonClick();
                                      }
                                  }
        );
        builder.setNegativeButton(baseActivity.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener()
                                  {
                                      public void onClick(DialogInterface dialog, int id)
                                      {
                                          dialog.cancel();
                                      }
                                  }
        );

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void show(Context baseActivity, String title, String message, String btnLeftText, String btnRightText,
                            final DialogInterface.OnClickListener btnLeftListener, final DialogInterface.OnClickListener btnRightListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity);

        builder
            .setMessage(message)
            .setTitle(title)
            .setPositiveButton(btnLeftText, btnLeftListener)
            .setNegativeButton(btnRightText, btnRightListener)
        ;

        if (btnLeftListener == null)
        {
            builder.setPositiveButton(btnLeftText, dismissListener);
        }

        if (btnRightListener == null)
        {
            builder.setNegativeButton(btnRightText, dismissListener);
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void show(Context context, String title, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message).setTitle(title);

        builder.setNegativeButton(context.getString(R.string.dialog_ok), new DialogInterface.OnClickListener()
                                  {
                                      public void onClick(DialogInterface dialog, int id)
                                      {
                                          dialog.cancel();
                                      }
                                  }
        );

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
