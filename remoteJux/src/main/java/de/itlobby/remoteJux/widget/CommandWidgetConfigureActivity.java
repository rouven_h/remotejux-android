package de.itlobby.remoteJux.widget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.adapters.CommandArrayAdapter;
import de.itlobby.remoteJux.models.CLICommandWrapper;
import de.itlobby.remoteJux.settings.Settings;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * The configuration screen for the {@link CommandWidget CommandWidget} AppWidget.
 */
public class CommandWidgetConfigureActivity extends Activity
{
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private CLICommandWrapper selectedCommand;

    public CommandWidgetConfigureActivity()
    {
        super();
    }

    @Override
    public void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);
        setContentView(R.layout.command_widget_configure);

        ListView listViewCommands = (ListView) findViewById(R.id.cfgWidget_lwCommandSelection);
        ArrayList<CLICommandWrapper> commandListItems = Settings.getInstance().loadCommands();
        final CommandArrayAdapter adapter = new CommandArrayAdapter(this, commandListItems, R.layout.command_widget_cfg_row_item);
        listViewCommands.setAdapter(adapter);
        listViewCommands.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);

        listViewCommands.setOnItemClickListener(
            new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                {
                    selectedCommand = adapter.getItem(position);

                    final Context context = CommandWidgetConfigureActivity.this;

                    HashMap<Integer, CLICommandWrapper> widgets = Settings.loadWidgets();
                    widgets.put(mAppWidgetId, selectedCommand);
                    Settings.saveWidgets(widgets);

                    AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                    CommandWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

                    Intent resultValue = new Intent();
                    resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                    setResult(RESULT_OK, resultValue);
                    finish();
                }
            }
        );

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null)
        {
            mAppWidgetId = extras.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID
            );
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID)
        {
            finish();
            return;
        }
    }
}



