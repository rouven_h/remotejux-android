package de.itlobby.remoteJux.widget;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.models.CLICommand;
import de.itlobby.remoteJux.models.CLIResponse;

public class SimpleWidgetDialogActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_widget_dialog);

        CLICommand widget_cli_command = (CLICommand) getIntent().getExtras().getSerializable("WIDGET_CLI_COMMAND");
        final CLIResponse widget_cli_response = (CLIResponse) getIntent().getExtras().getSerializable("WIDGET_CLI_RESPONSE");

        final TextView wdgt_dlg_resultText = (TextView) findViewById(R.id.wdgt_dlg_resultText);
        Button resultOk = (Button) findViewById(R.id.wdgt_dlg_resultOk);
        Button resultCopy = (Button) findViewById(R.id.wdgt_dlg_resultCopy);

        setTitle(getString(R.string.widget_resultDialog) + " " + widget_cli_command.getCommand());

        wdgt_dlg_resultText.setText(widget_cli_response.getResult());
        resultOk.setOnClickListener(
            new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent i = new Intent();
                    i.setAction(Intent.ACTION_MAIN);
                    i.addCategory(Intent.CATEGORY_HOME);
                    startActivity(i);
                    finish();
                }
            }
        );

        resultCopy.setOnClickListener(
            new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    copyToClipBoard(widget_cli_response);
                }
            }
        );
    }

    private void copyToClipBoard(CLIResponse cliResponse)
    {
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("simple text", cliResponse.getResult());
        clipboardManager.setPrimaryClip(clipData);
    }
}
