package de.itlobby.remoteJux.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.models.CLICommand;
import de.itlobby.remoteJux.models.CLICommandWrapper;
import de.itlobby.remoteJux.models.CLIResponse;
import de.itlobby.remoteJux.services.BackendService;
import de.itlobby.remoteJux.settings.Settings;
import de.itlobby.remoteJux.util.ExceptionUtil;
import de.itlobby.remoteJux.util.MapUtil;
import de.itlobby.remoteJux.util.SystemUtil;

import java.util.HashMap;


/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link CommandWidgetConfigureActivity CommandWidgetConfigureActivity}
 */
public class CommandWidget extends AppWidgetProvider
{
    public static String WIDGET_BUTTON = "de.itlobby.remoteJux.widget.WIDGET_BUTTON";

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId)
    {
        CLICommandWrapper cliCommandWrapper = Settings.loadWidgets().get(appWidgetId);

        if (cliCommandWrapper == null)
        {
            cliCommandWrapper = new CLICommandWrapper();
        }

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.command_widget);
        views.setTextViewText(R.id.widget_txtCmdLabel, cliCommandWrapper.getDisplayValue());

        if (cliCommandWrapper.getIconInfo() != null)
        {
            Drawable drawableFromApp = SystemUtil.getDrawableFromApp(context, cliCommandWrapper.getIconInfo());
            views.setImageViewBitmap(R.id.widget_imgCmdLogo, ((BitmapDrawable) drawableFromApp).getBitmap());
        }
        else
        {
            views.setImageViewBitmap(R.id.widget_imgCmdLogo, ((BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_launcher)).getBitmap());
        }

        if (cliCommandWrapper.getCliCommand() != null)
        {
            Intent btnExecIntent = new Intent(WIDGET_BUTTON);
            btnExecIntent.putExtra("WIDGET_CLI_COMMAND", MapUtil.toJSON(cliCommandWrapper.getCliCommand()));
            btnExecIntent.putExtra("WIDGET_ID", appWidgetId);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, appWidgetId, btnExecIntent, 0);

            views.setOnClickPendingIntent(R.id.widget_BaseLayout, pendingIntent);
            views.setOnClickPendingIntent(R.id.widget_imgCmdLogo, pendingIntent);
            views.setOnClickPendingIntent(R.id.widget_txtCmdLabel, pendingIntent);
        }

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
    {
        for (int appWidgetId : appWidgetIds)
        {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds)
    {
        for (int appWidgetId : appWidgetIds)
        {
            HashMap<Integer, CLICommandWrapper> widgets = Settings.loadWidgets();
            widgets.remove(appWidgetId);
            Settings.saveWidgets(widgets);
        }
    }

    @Override
    public void onReceive(final Context context, Intent intent)
    {
        Settings instance = Settings.getInstance();
        if (instance.getAppContext() == null)
        {
            instance.setAppContext(context);
        }

        super.onReceive(context, intent);

        if (WIDGET_BUTTON.equals(intent.getAction()))
        {
            String jsonString = intent.getExtras().getString("WIDGET_CLI_COMMAND");
            int widgetId = intent.getExtras().getInt("WIDGET_ID");

            showClickAnimation(context, widgetId);

            CLICommand cliCommand = MapUtil.toObject(jsonString, CLICommand.class);

            BackendService backendService = BackendService.getInstance();

            if (backendService.checkWebService())
            {
                final CLIResponse cliResponse = backendService.doInBackground(cliCommand);
                Log.i("INFO", "Executed Command from Widget: " + cliCommand.getCommand());

                showActivityDialog(cliCommand, cliResponse, context);
            }
            else
            {
                Log.w("WARNING", "cannot execute Command from widget, no connection to server.");
                Toast.makeText(context, context.getString(R.string.widget_noConnection), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showClickAnimation(final Context context, final int widgetId)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    AppWidgetManager mgr = AppWidgetManager.getInstance(context);
                    RemoteViews view = new RemoteViews(context.getPackageName(), R.layout.command_widget);

                    view.setInt(R.id.widget_BaseLayout, "setBackgroundResource", R.drawable.roundshape_clicked);
                    mgr.updateAppWidget(widgetId, view);

                    Thread.sleep(500);

                    view.setInt(R.id.widget_BaseLayout, "setBackgroundResource", R.drawable.roundshape);

                    mgr.updateAppWidget(widgetId, view);
                }
                catch (InterruptedException e)
                {
                    ExceptionUtil.logException(e);
                }
            }
        }
        ).start();
    }

    private void showActivityDialog(CLICommand cliCommand, CLIResponse cliResponse, Context context)
    {
        Intent intent = new Intent(context, SimpleWidgetDialogActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("WIDGET_CLI_COMMAND", cliCommand);
        intent.putExtra("WIDGET_CLI_RESPONSE", cliResponse);
        context.startActivity(intent);
    }


}


