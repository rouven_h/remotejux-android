package de.itlobby.remoteJux.activities;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.adapters.CommandArrayAdapter;
import de.itlobby.remoteJux.listener.CommandSavedEvent;
import de.itlobby.remoteJux.listener.EventProvider;
import de.itlobby.remoteJux.listener.SettingsSavedEvent;
import de.itlobby.remoteJux.models.CLICommandWrapper;
import de.itlobby.remoteJux.models.NotifyType;
import de.itlobby.remoteJux.services.BackendService;
import de.itlobby.remoteJux.settings.Settings;
import de.itlobby.remoteJux.widget.CommandWidget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.ActionMode.Callback;

public class MainActivity extends Activity implements Callback
{
    public static final String EDIT_MESSAGE = "EDIT_MESSAGE";
    private ArrayList<CLICommandWrapper> commandListItems;
    private CommandArrayAdapter adapter;
    private CLICommandWrapper longPressedItem;
    private View longPressedItemView;
    private Menu menu;

    public MainActivity()
    {
        new Timer().schedule(
            new TimerTask()
            {
                @Override
                public void run()
                {
                    checkServerConnection();
                }
            }, 1500, 30000
        );

        //For Network access
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Settings.getInstance().setAppContext(this);

        initializeView();
    }

    private void initializeView()
    {
        ListView listViewCommands = (ListView) findViewById(R.id.lwCommands);
        commandListItems = Settings.getInstance().loadCommands();
        adapter = new CommandArrayAdapter(this, commandListItems, R.layout.command_row_item);
        listViewCommands.setAdapter(adapter);

        listViewCommands.setOnItemLongClickListener(
            new AdapterView.OnItemLongClickListener()
            {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
                {
                    ActionMode actionMode = startActionMode(MainActivity.this);

                    if (longPressedItem != null && longPressedItemView != null && actionMode != null)
                    {
                        if (adapter.getPosition(longPressedItem) == position)
                        {
                            view.setSelected(false);
                            longPressedItem = null;
                            longPressedItemView = null;
                            actionMode.finish();
                        }

                        return false;
                    }

                    longPressedItem = adapter.getItem(position);
                    longPressedItemView = view;
                    view.setSelected(true);
                    return true;
                }
            }
        );

        Button btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(
            new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    showAddCommandView();
                }
            }
        );

        EventProvider.settingsSavedEvent = new SettingsSavedEvent()
        {
            @Override
            public void settingsSaved()
            {
                applySettings();
            }
        };
    }

    private void showSettingsView()
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void applySettings()
    {
        checkServerConnection();
        System.out.println("Settings reloaded");
    }

    private void onEditCommand()
    {
        final int tmpPosition = commandListItems.indexOf(longPressedItem);
        final CLICommandWrapper tmpObj = longPressedItem;

        EventProvider.commandSavedEvent = new CommandSavedEvent()
        {
            @Override
            public void commandSaved(CLICommandWrapper editedCommandWrapper)
            {
                commandListItems.remove(tmpPosition);
                commandListItems.add(tmpPosition, editedCommandWrapper);
                onNotifyDataSetChanged();

                notifyWidgets(tmpObj, editedCommandWrapper, NotifyType.EDIT);
            }
        };

        Intent intent = new Intent(this, AddCommandActivity.class);
        intent.putExtra(EDIT_MESSAGE, longPressedItem);
        startActivity(intent);
    }

    private void notifyWidgets(CLICommandWrapper oldCommandWrapper, CLICommandWrapper editedCommandWrapper, NotifyType notifyType)
    {
        if (notifyType == NotifyType.EDIT)
        {
            HashMap<Integer, CLICommandWrapper> widgets = Settings.loadWidgets();

            HashMap<Integer, CLICommandWrapper> toUpdate = new HashMap<>();

            for (Map.Entry<Integer, CLICommandWrapper> entry : widgets.entrySet())
            {
                if (entry.getValue().equals(oldCommandWrapper))
                {
                    toUpdate.put(entry.getKey(), editedCommandWrapper);
                }
            }

            for (Map.Entry<Integer, CLICommandWrapper> entry : toUpdate.entrySet())
            {
                widgets.put(entry.getKey(), entry.getValue());
            }

            Settings.saveWidgets(widgets);

            for (Map.Entry<Integer, CLICommandWrapper> entry : toUpdate.entrySet())
            {
                CommandWidget.updateAppWidget(this, AppWidgetManager.getInstance(this), entry.getKey());
            }
        }
    }

    private void onNotifyDataSetChanged()
    {
        adapter.notifyDataSetChanged();
        Settings.getInstance().saveCommands(commandListItems);
    }

    private void onRemoveCommand()
    {
        commandListItems.remove(longPressedItem);
        onNotifyDataSetChanged();
    }

    private void showAddCommandView()
    {
        EventProvider.commandSavedEvent = new CommandSavedEvent()
        {
            @Override
            public void commandSaved(CLICommandWrapper cliCommandWrapper)
            {
                commandListItems.add(cliCommandWrapper);
                onNotifyDataSetChanged();
            }
        };

        Intent intent = new Intent(this, AddCommandActivity.class);
        startActivity(intent);
    }

    private void checkServerConnection()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                final boolean isOnline = BackendService.getInstance().checkWebService();

                if (menu == null || menu.findItem(R.id.action_conntection) == null)
                {
                    return;
                }

                final MenuItem conItem = menu.findItem(R.id.action_conntection);
                final Drawable icon = conItem.getIcon();

                runOnUiThread(
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (isOnline)
                            {
                                Log.i("INFO", "Server is online");
                                icon.setColorFilter(0xff00ff00, PorterDuff.Mode.MULTIPLY);
                            }
                            else
                            {
                                Log.i("INFO", "Server is offline");
                                icon.setColorFilter(0xffff0000, PorterDuff.Mode.MULTIPLY);
                            }

                            MainActivity.this.invalidateOptionsMenu();
                        }
                    }
                );
            }
        }
        ).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings)
        {
            showSettingsView();
            return true;
        }
        else if (id == R.id.action_conntection)
        {
            checkServerConnection();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu)
    {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.commands_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu)
    {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_action_edit:
                onEditCommand();
                break;
            case R.id.menu_action_remove:
                onRemoveCommand();
                break;
            default:
                return false;
        }

        mode.finish();
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode)
    {
        if (longPressedItemView != null)
        {
            longPressedItemView.setSelected(false);
            longPressedItemView = null;
            longPressedItem = null;
        }
    }
}
