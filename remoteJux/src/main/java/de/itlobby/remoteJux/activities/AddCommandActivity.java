package de.itlobby.remoteJux.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.dialogs.ComboDialog;
import de.itlobby.remoteJux.dialogs.MessageDialog;
import de.itlobby.remoteJux.listener.EventProvider;
import de.itlobby.remoteJux.listener.IconSelectorSelectEvent;
import de.itlobby.remoteJux.models.CLICommand;
import de.itlobby.remoteJux.models.CLICommandWrapper;
import de.itlobby.remoteJux.models.IconInfo;
import de.itlobby.remoteJux.util.StringUtil;
import de.itlobby.remoteJux.util.SystemUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class AddCommandActivity extends Activity
{
    private IconInfo selectedIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_command);
        selectedIcon = null;

        Button btnSaveCommand = (Button) findViewById(R.id.btnSaveCommand);
        btnSaveCommand.setOnClickListener(
            new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    onBtnSaveCommand();
                }
            }
        );

        Button btnSelectIcon = (Button) findViewById(R.id.btnSelectIcon);
        btnSelectIcon.setOnClickListener(new View.OnClickListener()
                                         {
                                             @Override
                                             public void onClick(View v)
                                             {
                                                 onSelectIcon();
                                             }
                                         }
        );

        CLICommandWrapper originalWrapper = (CLICommandWrapper) getIntent().getSerializableExtra(MainActivity.EDIT_MESSAGE);

        if (originalWrapper != null)
        {
            EditText txtName = (EditText) findViewById(R.id.txtName);
            txtName.setText(originalWrapper.getDisplayValue());

            EditText txtCommand = (EditText) findViewById(R.id.txtCommand);
            txtCommand.setText(originalWrapper.getCliCommand().getCommand());

            CheckBox chkReturnsText = (CheckBox) findViewById(R.id.chkReturnsText);
            chkReturnsText.setChecked(originalWrapper.getCliCommand().isReturnsText());

            CheckBox chkRequiresUserInput = (CheckBox) findViewById(R.id.chkRequiresUserInput);
            chkRequiresUserInput.setChecked(originalWrapper.getCliCommand().isRequiresInput());

            ImageView imgIcon = (ImageView) findViewById(R.id.imgIcon);
            imgIcon.setImageDrawable(SystemUtil.getDrawableFromApp(this, originalWrapper.getIconInfo()));
            selectedIcon = originalWrapper.getIconInfo();

            this.setTitle(getString(R.string.title_activity_editcommand));
        }
    }

    private void onSelectIcon()
    {
        List<IconInfo> allInstalledAppIcons = SystemUtil.getAllInstalledAppIcons(this);

        Collections.sort(allInstalledAppIcons, new Comparator<IconInfo>()
                         {
                             @Override
                             public int compare(IconInfo lhs, IconInfo rhs)
                             {
                                 return lhs.getName().compareTo(rhs.getName());
                             }
                         }
        );

        ComboDialog.showIconSelctor(this, getString(R.string.addCommand_SelectIcon), allInstalledAppIcons);

        EventProvider.iconSelectorSelectEvent = new IconSelectorSelectEvent()
        {
            @Override
            public void itemSelected(IconInfo menuItem)
            {
                selectedIcon = menuItem;

                ImageView imgIcon = (ImageView) findViewById(R.id.imgIcon);
                imgIcon.setImageDrawable(SystemUtil.getDrawableFromApp(getApplicationContext(), menuItem));
            }
        };
    }

    private void onBtnSaveCommand()
    {
        EditText txtName = (EditText) findViewById(R.id.txtName);
        EditText txtCommand = (EditText) findViewById(R.id.txtCommand);
        CheckBox chkRequiresUserInput = (CheckBox) findViewById(R.id.chkRequiresUserInput);
        CheckBox chkReturnsText = (CheckBox) findViewById(R.id.chkReturnsText);

        if (StringUtil.isNullOrEmpty(txtName.getText().toString()) || StringUtil.isNullOrEmpty(txtCommand.getText().toString()))
        {
            MessageDialog.show(this, getString(R.string.addCommand_InputError), getString(R.string.addCommand_fillAllFields));
            return;
        }

        CLICommand cliCommand = new CLICommand(txtCommand.getText().toString(), chkRequiresUserInput.isChecked(), chkReturnsText.isChecked());
        CLICommandWrapper cliCommandWrapper = new CLICommandWrapper(cliCommand);
        cliCommandWrapper.setDisplayValue(txtName.getText().toString());
        cliCommandWrapper.setIconInfo(selectedIcon);

        EventProvider.commandSavedEvent.commandSaved(cliCommandWrapper);

        this.finish();
    }
}
