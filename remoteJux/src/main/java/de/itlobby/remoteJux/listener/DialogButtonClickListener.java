package de.itlobby.remoteJux.listener;

public interface DialogButtonClickListener
{
    void onButtonClick();
}
