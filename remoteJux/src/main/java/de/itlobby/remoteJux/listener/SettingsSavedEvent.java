package de.itlobby.remoteJux.listener;

public interface SettingsSavedEvent
{
    void settingsSaved();
}
