package de.itlobby.remoteJux.listener;

import de.itlobby.remoteJux.models.IconInfo;

public interface IconSelectorSelectEvent
{
    void itemSelected(IconInfo menuItem);
}
