package de.itlobby.remoteJux.listener;

public class EventProvider
{
    public static CommandSavedEvent commandSavedEvent;

    public static SettingsSavedEvent settingsSavedEvent;

    public static IconSelectorSelectEvent iconSelectorSelectEvent;
}
