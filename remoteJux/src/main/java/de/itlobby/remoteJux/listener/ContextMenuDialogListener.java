package de.itlobby.remoteJux.listener;

import de.itlobby.remoteJux.dialogs.ContextMenuItem;

public interface ContextMenuDialogListener
{
    void itemSelected(ContextMenuItem menuItem);
}
