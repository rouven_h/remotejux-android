package de.itlobby.remoteJux.listener;

import de.itlobby.remoteJux.models.CLICommandWrapper;

public interface CommandSavedEvent
{
    void commandSaved(CLICommandWrapper cliCommandWrapper);
}
