package de.itlobby.remoteJux.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import de.itlobby.remoteJux.models.CLICommandWrapper;
import de.itlobby.remoteJux.util.ExceptionUtil;
import de.itlobby.remoteJux.util.MapUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Settings
{
    public static final String COMMANDS_NAME = "REMOTEJUX_COMMANDS";
    public static final String COMMANDS_CONST = "COMMANDS";

    public static final String WIDGETS_NAME = "REMOTEJUX_WIDGETS";
    public static final String WIDGETS_CONST = "WIDGETS";
    private static Settings instance;
    private Context appContext;

    private Settings()
    {

    }

    public static Settings getInstance()
    {
        if (instance == null)
        {
            instance = new Settings();
        }

        return instance;
    }

    public static HashMap<Integer, CLICommandWrapper> loadWidgets()
    {
        try
        {
            SharedPreferences settings = getInstance().appContext.getSharedPreferences(WIDGETS_NAME, 0);
            HashMap<Integer, CLICommandWrapper> ret = new HashMap<>();

            Set<String> settingsSet = settings.getStringSet(WIDGETS_CONST, new HashSet<String>());

            ret.putAll(MapUtil.mapJsonToWidgets(settingsSet));

            return ret;
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }

        return new HashMap<>();
    }

    public static void saveWidgets(HashMap<Integer, CLICommandWrapper> widgets)
    {
        try
        {
            SharedPreferences settings = getInstance().appContext.getSharedPreferences(WIDGETS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();

            Set<String> jsonSet = MapUtil.mapWidgetsToJsonWidgets(widgets);

            editor.putStringSet(WIDGETS_CONST, jsonSet);

            editor.commit();
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }
    }

    public AppConfig getConfig()
    {
        try
        {
            AppConfig appConfig = new AppConfig();

            if (appContext == null)
            {
                return appConfig;
            }

            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(appContext);

            for (Field field : appConfig.getClass().getDeclaredFields())
            {
                String fieldName = field.getName();
                String settingsValue = settings.getString(fieldName, null);

                if (settingsValue == null)
                {
                    continue;
                }

                if (field.getType().equals(String.class))
                {
                    field.set(appConfig, settingsValue);
                }
                else if (field.getType().equals(Integer.class))
                {
                    field.set(appConfig, Integer.parseInt(settingsValue));
                }
                else if (field.getType().equals(Double.class))
                {
                    field.set(appConfig, Double.parseDouble(settingsValue));
                }
                else if (field.getType().equals(Boolean.class))
                {
                    field.set(appConfig, Boolean.parseBoolean((settingsValue)));
                }
                else
                {
                    throw new IllegalStateException("Unhandled settings type " + field.getType());
                }
            }

            return appConfig;
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }

        return null;
    }

    public Context getAppContext()
    {
        return appContext;
    }

    public void setAppContext(Context appContext)
    {
        this.appContext = appContext;
    }

    public ArrayList<CLICommandWrapper> loadCommands()
    {
        try
        {
            SharedPreferences settings = appContext.getSharedPreferences(COMMANDS_NAME, 0);
            ArrayList<CLICommandWrapper> ret = new ArrayList<>();

            Set<String> commandSet = settings.getStringSet(COMMANDS_CONST, new HashSet<String>());

            ret.addAll(MapUtil.mapJsonToCommands(commandSet));

            return ret;
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }

        return new ArrayList<>();
    }

    public void saveCommands(List<CLICommandWrapper> commands)
    {
        try
        {
            SharedPreferences settings = appContext.getSharedPreferences(COMMANDS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();

            Set<String> jsonCmds = MapUtil.mapCommandsToJsonCommands(commands);

            editor.putStringSet(COMMANDS_CONST, jsonCmds);

            editor.commit();
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }
    }
}
