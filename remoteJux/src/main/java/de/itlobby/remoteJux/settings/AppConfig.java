package de.itlobby.remoteJux.settings;

public class AppConfig
{
    // DO NOT USE SIMPLE DATATYPES AND MAKE NOT PRIVATE
    String serverName = "localhost";
    String serverPort = "9998";

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getServerPort()
    {
        return serverPort;
    }

    public void setServerPort(String serverPort)
    {
        this.serverPort = serverPort;
    }
}
