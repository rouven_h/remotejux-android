package de.itlobby.remoteJux.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.dialogs.MessageDialog;
import de.itlobby.remoteJux.models.CLICommand;
import de.itlobby.remoteJux.models.CLICommandWrapper;
import de.itlobby.remoteJux.models.CLIResponse;
import de.itlobby.remoteJux.services.BackendService;
import de.itlobby.remoteJux.settings.AppConfig;
import de.itlobby.remoteJux.settings.Settings;
import de.itlobby.remoteJux.util.StringUtil;
import de.itlobby.remoteJux.util.SystemUtil;

import java.util.List;

public class CommandArrayAdapter extends ArrayAdapter<CLICommandWrapper>
{
    private final Context context;
    private final List<CLICommandWrapper> values;
    private final int rowLayout;

    public CommandArrayAdapter(Context context, List<CLICommandWrapper> values, int rowLayout)
    {
        super(context, rowLayout, values);
        this.rowLayout = rowLayout;
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(rowLayout, parent, false);

        final CLICommandWrapper currentRowItem = values.get(position);

        ImageView cmdRowItemIcon = null;
        TextView cmdRowItemLabel = null;
        TextView cmdRowItemCommand = null;
        Button cmdRowItemBtnExecute = null;

        if (rowLayout == R.layout.command_row_item)
        {
            cmdRowItemIcon = (ImageView) rowView.findViewById(R.id.cmdRowItemIcon);
            cmdRowItemLabel = (TextView) rowView.findViewById(R.id.cmdRowItemLabel);
            cmdRowItemCommand = (TextView) rowView.findViewById(R.id.cmdRowItemCommand);
            cmdRowItemBtnExecute = (Button) rowView.findViewById(R.id.cmdRowItemBtnExecute);
        }
        else
        {
            cmdRowItemIcon = (ImageView) rowView.findViewById(R.id.widget_cmdRowItemIcon);
            cmdRowItemLabel = (TextView) rowView.findViewById(R.id.widget_cmdRowItemLabel);
            cmdRowItemCommand = (TextView) rowView.findViewById(R.id.widget_cmdRowItemCommand);
        }

        if (currentRowItem.getIconInfo() != null)
        {
            cmdRowItemIcon.setImageDrawable(SystemUtil.getDrawableFromApp(context, currentRowItem.getIconInfo()));
        }

        cmdRowItemLabel.setText(currentRowItem.getDisplayValue());
        cmdRowItemCommand.setText(currentRowItem.getCliCommand().getCommand());

        if (cmdRowItemBtnExecute != null)
        {
            cmdRowItemBtnExecute.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        handleOnCommandExecute(currentRowItem, context);
                    }
                }
            );
        }

        return rowView;
    }

    private void handleOnCommandExecute(CLICommandWrapper currentRowItem, Context context)
    {
        CLICommand cliCommand = currentRowItem.getCliCommand();

        CLIResponse cliResponse = null;

        Settings settings = Settings.getInstance();

        if (BackendService.getInstance().checkWebService())
        {
            cliResponse = BackendService.getInstance().doInBackground(cliCommand);
        }
        else
        {
            AppConfig config = settings.getConfig();
            MessageDialog.show(settings.getAppContext(),
                               context.getString(R.string.commandResult_serverNotFound),
                               context.getString(R.string.commandResult_noConnection) + " " + config.getServerName() + ":" + config.getServerPort()
            );
        }

        if (cliResponse != null && cliCommand.isReturnsText() && !StringUtil.isNullOrEmpty(cliResponse.getResult()))
        {
            MessageDialog.show(settings.getAppContext(),
                               context.getString(R.string.commandResult_resultOf) + " " + currentRowItem.getDisplayValue(),
                               cliResponse.getResult()
            );
        }
    }
}