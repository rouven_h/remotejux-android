package de.itlobby.remoteJux.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import de.itlobby.remoteJux.R;
import de.itlobby.remoteJux.models.IconInfo;
import de.itlobby.remoteJux.util.SystemUtil;

import java.util.List;

public class ComboIconArrayAdapter extends ArrayAdapter<IconInfo>
{
    private final Context context;
    private final List<IconInfo> values;

    public ComboIconArrayAdapter(Context context, List<IconInfo> values)
    {
        super(context, R.layout.combo_box_icon_item, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.combo_box_icon_item, parent, false);

        IconInfo currentRowItem = values.get(position);

        ImageView cbmIconimage = (ImageView) rowView.findViewById(R.id.cbmIconimage);
        TextView cbmIconLabel = (TextView) rowView.findViewById(R.id.cbmIconLabel);

        cbmIconimage.setImageDrawable(SystemUtil.getDrawableFromApp(context, currentRowItem));
        cbmIconLabel.setText(currentRowItem.getName());

        return rowView;
    }
}