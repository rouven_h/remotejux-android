package de.itlobby.remoteJux.framework;

import java.lang.reflect.Constructor;
import java.util.HashMap;

public class ServiceLocator
{
    private static ServiceLocator instance;
    private HashMap<Class, Object> services = new HashMap<>();

    private ServiceLocator()
    {
    }

    public static <T> T getServiceInstance(Class<T> clazz)
    {
        if (instance == null)
        {
            instance = new ServiceLocator();
        }

        return instance.getService(clazz);
    }

    private <T> T getService(Class<T> clazz)
    {
        T obj = (T) services.get(clazz);

        if (obj == null)
        {
            obj = (T) createServiceInstance(clazz);
        }

        return obj;
    }

    private Object createServiceInstance(Class clazz)
    {
        Object obj = null;
        try
        {
            Constructor<?> ctor = clazz.getConstructor();
            obj = ctor.newInstance();

            services.put(clazz, obj);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return obj;
    }
}
